// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//

#pragma once

#ifndef stdafx_H_
#define stdafx_H_
#include <atlbase.h>
#include <atlapp.h>

extern CAppModule _Module;
#include <atlcom.h>
#include <atlhost.h>
#include <atlwin.h>
#include <atlctl.h>

#include <atlframe.h>
#include <atlctrls.h>
#include <atldlgs.h>
#include <string>
#include <vector>
#include <list>

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <process.h>
#include <Windows.h>
#include <assert.h>
#endif

//soui包含文件
#include <souistd.h>
#include <core/SHostDialog.h>
#include <control/SMessageBox.h>
#include <control/souictrls.h>
#include <helper/SMenuEx.h>
#include <com-cfg.h>
#include <unknown/obj-ref-impl.hpp>
#include <unknown/obj-ref-i.h>
#include <interface/SResProvider-i.h>
#include "resource.h"
// TODO: 在此处引用程序需要的其他头文件
